P, Q = 53, 59
N = P * Q

def keys(e, p = P, q = Q):
  encKey = e
  n = p * q
  phi = (p - 1) * (q - 1)
  E, D, d = phi, phi, 1
  while (e != 1):
    e, d, E, D = E % e, (D - E // e * d) % phi, e, d
  print([encKey, d, n])
  return [encKey, d, n]

def crypt(m, key, n = N):
  return m**key % n

