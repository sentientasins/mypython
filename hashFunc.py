def shr(x, SHR = 32):
  i, r = 0, ''
  while (i < SHR):
    r += '0'
    i += 1
  return r + x[0 : len(x) - SHR]

def rotr(x, rotr = 32):
  return x[len(x) - rotr : len(x)] + x[0 : len(x) - rotr]

def xor(*args):
  x = [y for y in args]
  while (len(x) > 1):
    i = 0
    lastLine = x[len(x) - 1]
    while (i < len(lastLine)):
      x[len(x) - 2] = x[len(x) - 2][0 : i] + ('0' if x[len(x) - 2][i] == x[len(x) - 1][i] else '1') + x[len(x) - 2][i + 1 : len(lastLine)]
      i += 1
    x.remove(lastLine)
  return x[0]

def add(*args):
  x = [y for y in args]
  x[len(x) - 1] = int(x[len(x) - 1], 2)
  while (len(x) > 1):
    a, b = x[len(x) - 2], x[len(x) - 1]
    x[len(x) - 2] = int(a, 2) + b
    x.remove(b)
  return format(x[0] % 2**32, '032b')

def sigma0(x):
  return xor(rotr(x, 7), rotr(x, 18), shr(x, 3))

def sigma1(x):
  return xor(rotr(x, 17), rotr(x, 19), shr(x, 10))

def usigma0(x):
  return xor(rotr(x, 2), rotr(x, 13), rotr(x, 22))

def usigma1(x):
  return xor(rotr(x, 6), rotr(x, 11), rotr(x, 25))

def ch(x, y, z):
  i, r = 0, ''
  while (i < len(x)):
    r = r + (y[i] if x[i] == '1' else z[i])
    i += 1
  return r

def maj(x, y, z):
  i, r = 0, ''
  while (i < len(x)):
    r = r + ('1' if int(x[i]) + int(y[i]) + int(z[i]) > 1 else '0')
    i += 1
  return r

def sievePrime(n = 311):
  Lprimes = [x for x in list(range(2, n + 1))]
  for i in Lprimes:
    for k in range(i*i, n + 1, i):
      if k % i == 0 and k in Lprimes: Lprimes.remove(k)
  return Lprimes

K = [format(int((x**(1/3) % 1) * 2**32), '032b') for x in sievePrime()[0 : 64]]

def textToBin(message):
  x = ''
  for i in message:
    x = x + format(ord(i), '008b')
  return x

def padding(message):
  M = len(message)
  message += '1'
  i, length = 0, (448 - len(message)) % 512
  while (i < length):
    message += '0'
    i += 1
  message += format(M, '064b')
  return message

def parsing(message, length = 512):
  x = []
  while (len(message) != 0):
    x.append(message[0 : length])
    message = message[length : len(message)]
  return x

def scheduling(message):
  W = parsing(message, 32)
  i = 16
  while (i < 64):
    W.append(add(sigma1(W[i - 2]), W[i - 7], sigma0(W[i - 15]), W[i - 16]))
    i += 1
  return W

initialHash = [format(int((x**0.5 % 1) * 2**32), '032b') for x in sievePrime(20)[0 : 8]]

def compression(W, H0 = initialHash):
  H1, i = H0.copy(), 0
  while (i < 64):
    T1 = add(usigma1(H0[4]), ch(H0[4], H0[5], H0[6]), H0[7], K[i], W[i])
    T2 = add(usigma0(H0[0]), maj(H0[0], H0[1], H0[2]))
    H0[1], H0[2], H0[3], H0[4], H0[5], H0[6], H0[7] = H0[0], H0[1], H0[2], H0[3], H0[4], H0[5], H0[6]
    H0[0], H0[4] = add(T1, T2), add(H0[4], T1)
    i += 1
  i = 0
  while (i < 8):
    H1[i] = add(H0[i], H1[i])
    i += 1
  return H1

def binToHash(H):
  x = ''
  for h in H:
    x += format(int(h, 2), '08x')
  return x

def SHA256(message):
  message = textToBin(message)
  message = padding(message)
  message = parsing(message)
  H0 = initialHash
  for block in message:
    schedule = scheduling(block)
    H0 = compression(schedule, H0)
  message = binToHash(H0)
  return message

